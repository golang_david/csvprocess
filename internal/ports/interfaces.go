package ports

type Service interface {
	Run(isSimulation bool, withTime bool) error
}
