package test

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/golang_david/csvprocess/internal/core/model/scripts"
	"testing"
)

func TestNewCSV(t *testing.T) {
	tests := []struct {
		name   string
		task   string
		errMsg string
	}{
		{
			name: "new csv success",
			task: `
[
	{
		"name": "test csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
		},
		{
			name: "new csv without name",
			task: `
[
	{
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			errMsg: "name is required",
		},
		{
			name: "new csv without order",
			task: `
[
	{
		"name": "test csv",
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			errMsg: "order is required",
		},
		{
			name: "new csv without MaxRowProcess",
			task: `
[
	{
		"name": "test csv",
		"order": 1,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
		},
		{
			name: "new csv without type",
			task: `
[
	{
		"name": "test csv",
		"order": 1,
		"max_row_process": 0,
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			errMsg: "missing type",
		},
		{
			name: "new csv with unknown type",
			task: `
[
	{
		"name": "test csv",
		"order": 1,
		"max_row_process": 0,
		"type": "unknown_type",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			errMsg: "unknown type unknown_type",
		},
		{
			name: "new csv without input_file",
			task: `
[
	{
		"name": "test csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			errMsg: "file is required",
		},
		{
			name: "new csv with file without extension csv",
			task: `
[
	{
		"name": "test csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input.txt",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			errMsg: "extension 'txt' is not valid",
		},
		{
			name: "new csv without has_header",
			task: `
[
	{
		"name": "test csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"delimiter": ","
	}
]`,
		},
		{
			name: "new csv without delimiter",
			task: `
[
	{
		"name": "test csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true
	}
]`,
			errMsg: "delimiter is required",
		},
		{
			name: "new csv with delimiter invalid",
			task: `
[
	{
		"name": "test csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": "\n"
	}
]`,
			errMsg: "delimiter (\n) is not supported",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tasks := make([]scripts.Task, 0)
			err := json.Unmarshal([]byte(tt.task), &tasks)
			if err != nil {
				assert.Equal(t, tt.errMsg, err.Error())
			} else if tt.errMsg != "" {
				t.Fatal("error not found")
			}
		})
	}
}

func TestNewAPI(t *testing.T) {
	tests := []struct {
		name   string
		task   string
		errMsg string
	}{
		{
			name: "api success",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"max_row_process": 0,
		"type": "api",
		"filters": [
			{
				"header": "some_header",
				"operation": "==",
				"value": "some_value"
			}
		],
		"columns": ["header1", "header2"],
		"workers": 3,
		"url": "http://testing.test/something",
		"body_template": "./resource/body.json",
		"mock_response": "./resource/mock.json",
		"method": "POST",
		"root_list": "something",
		"headers": {
			"some_header": "some_value"
		},
		"params": {
			"some_param": "some_value"
		},
		"rest_client": {
			"rate": "rps",
			"init": 50,
			"limit": 50,
			"increment_percentage": 10,
			"increment_seconds": 5,
			"retries": 1
		}
	}
]`,
		},
		{
			name: "api without name",
			task: `
[
	{
		"order": 1,
		"max_row_process": 0,
		"type": "api",
		"filters": [
			{
				"header": "some_header",
				"operation": "==",
				"value": "some_value"
			}
		],
		"columns": ["header1", "header2"],
		"workers": 3,
		"url": "http://testing.test/something",
		"body_template": "./resource/body.json",
		"mock_response": "./resource/mock.json",
		"method": "POST",
		"root_list": "something",
		"headers": {
			"some_header": "some_value"
		},
		"params": {
			"some_param": "some_value"
		},
		"rest_client": {
			"rate": "rps",
			"init": 50,
			"limit": 50,
			"increment_percentage": 10,
			"increment_seconds": 5,
			"retries": 1
		}
	}
]`,
			errMsg: "name is required",
		},
		{
			name: "api without order",
			task: `
[
	{
		"name": "test script api",
		"max_row_process": 0,
		"type": "api",
		"filters": [
			{
				"header": "some_header",
				"operation": "==",
				"value": "some_value"
			}
		],
		"columns": ["header1", "header2"],
		"workers": 3,
		"url": "http://testing.test/something",
		"body_template": "./resource/body.json",
		"mock_response": "./resource/mock.json",
		"method": "POST",
		"root_list": "something",
		"headers": {
			"some_header": "some_value"
		},
		"params": {
			"some_param": "some_value"
		},
		"rest_client": {
			"rate": "rps",
			"init": 50,
			"limit": 50,
			"increment_percentage": 10,
			"increment_seconds": 5,
			"retries": 1
		}
	}
]`,
			errMsg: "order is required",
		},
		{
			name: "api without max_row_process",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"type": "api",
		"filters": [
			{
				"header": "some_header",
				"operation": "==",
				"value": "some_value"
			}
		],
		"columns": ["header1", "header2"],
		"workers": 3,
		"url": "http://testing.test/something",
		"body_template": "./resource/body.json",
		"mock_response": "./resource/mock.json",
		"method": "POST",
		"root_list": "something",
		"headers": {
			"some_header": "some_value"
		},
		"params": {
			"some_param": "some_value"
		},
		"rest_client": {
			"rate": "rps",
			"init": 50,
			"limit": 50,
			"increment_percentage": 10,
			"increment_seconds": 5,
			"retries": 1
		}
	}
]`,
		},
		{
			name: "api without type",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"max_row_process": 0,
		"filters": [
			{
				"header": "some_header",
				"operation": "==",
				"value": "some_value"
			}
		],
		"columns": ["header1", "header2"],
		"workers": 3,
		"url": "http://testing.test/something",
		"body_template": "./resource/body.json",
		"mock_response": "./resource/mock.json",
		"method": "POST",
		"root_list": "something",
		"headers": {
			"some_header": "some_value"
		},
		"params": {
			"some_param": "some_value"
		},
		"rest_client": {
			"rate": "rps",
			"init": 50,
			"limit": 50,
			"increment_percentage": 10,
			"increment_seconds": 5,
			"retries": 1
		}
	}
]`,
			errMsg: "missing type",
		},
		{
			name: "api with unknown type",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"max_row_process": 0,
		"type": "unknown_type",
		"filters": [
			{
				"header": "some_header",
				"operation": "==",
				"value": "some_value"
			}
		],
		"columns": ["header1", "header2"],
		"workers": 3,
		"url": "http://testing.test/something",
		"body_template": "./resource/body.json",
		"mock_response": "./resource/mock.json",
		"method": "POST",
		"root_list": "something",
		"headers": {
			"some_header": "some_value"
		},
		"params": {
			"some_param": "some_value"
		},
		"rest_client": {
			"rate": "rps",
			"init": 50,
			"limit": 50,
			"increment_percentage": 10,
			"increment_seconds": 5,
			"retries": 1
		}
	}
]`,
			errMsg: "unknown type unknown_type",
		},
		{
			name: "api without url",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"max_row_process": 0,
		"type": "api",
		"filters": [
			{
				"header": "some_header",
				"operation": "==",
				"value": "some_value"
			}
		],
		"columns": ["header1", "header2"],
		"workers": 3,
		"body_template": "./resource/body.json",
		"mock_response": "./resource/mock.json",
		"method": "POST",
		"root_list": "something",
		"headers": {
			"some_header": "some_value"
		},
		"params": {
			"some_param": "some_value"
		},
		"rest_client": {
			"rate": "rps",
			"init": 50,
			"limit": 50,
			"increment_percentage": 10,
			"increment_seconds": 5,
			"retries": 1
		}
	}
]`,
			errMsg: "url is required",
		},
		{
			name: "api without method",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"max_row_process": 0,
		"type": "api",
		"filters": [
			{
				"header": "some_header",
				"operation": "==",
				"value": "some_value"
			}
		],
		"columns": ["header1", "header2"],
		"workers": 3,
		"url": "http://testing.test/something",
		"body_template": "./resource/body.json",
		"mock_response": "./resource/mock.json",
		"root_list": "something",
		"headers": {
			"some_header": "some_value"
		},
		"params": {
			"some_param": "some_value"
		},
		"rest_client": {
			"rate": "rps",
			"init": 50,
			"limit": 50,
			"increment_percentage": 10,
			"increment_seconds": 5,
			"retries": 1
		}
	}
]`,
			errMsg: "method is required",
		},
		{
			name: "api with method not available",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"max_row_process": 0,
		"type": "api",
		"filters": [
			{
				"header": "some_header",
				"operation": "==",
				"value": "some_value"
			}
		],
		"columns": ["header1", "header2"],
		"workers": 3,
		"url": "http://testing.test/something",
		"body_template": "./resource/body.json",
		"mock_response": "./resource/mock.json",
		"method": "SOME",
		"root_list": "something",
		"headers": {
			"some_header": "some_value"
		},
		"params": {
			"some_param": "some_value"
		},
		"rest_client": {
			"rate": "rps",
			"init": 50,
			"limit": 50,
			"increment_percentage": 10,
			"increment_seconds": 5,
			"retries": 1
		}
	}
]`,
			errMsg: "'SOME' is not available method",
		},
		{
			name: "api success with minimal options",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"type": "api",
		"columns": ["id"],
		"url": "http://testing.test/something",
		"method": "GET"
	}
]`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tasks := make([]scripts.Task, 0)
			err := json.Unmarshal([]byte(tt.task), &tasks)
			if err != nil {
				assert.Equal(t, tt.errMsg, err.Error())
			} else if tt.errMsg != "" {
				t.Fatal("error not found")
			}
		})
	}
}

func TestNewJSON(t *testing.T) {
	tests := []struct {
		name   string
		task   string
		errMsg string
	}{
		{
			name: "new json success",
			task: `
[
	{
		"name": "test json",
		"order": 1,
		"max_row_process": 0,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "json",
		"input_file": "./resource/input.json"
	}
]`,
		},
		{
			name: "new json without name",
			task: `
[
	{
		"order": 1,
		"max_row_process": 0,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "json",
		"input_file": "./resource/input.json"
	}
]`,
			errMsg: "name is required",
		},
		{
			name: "new json without order",
			task: `
[
	{
		"name": "test json",
		"max_row_process": 0,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "json",
		"input_file": "./resource/input.json"
	}
]`,
			errMsg: "order is required",
		},
		{
			name: "new json without max_process_row",
			task: `
[
	{
		"name": "test json",
		"order": 1,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "json",
		"input_file": "./resource/input.json"
	}
]`,
		},
		{
			name: "new json without file_input",
			task: `
[
	{
		"name": "test json",
		"order": 1,
		"max_row_process": 0,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "json"
	}
]`,
			errMsg: "file is required",
		},
		{
			name: "new json with file_input without extension",
			task: `
[
	{
		"name": "test json",
		"order": 1,
		"max_row_process": 0,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "json",
		"input_file": "./resource/input"
	}
]`,
			errMsg: "file without extension",
		},
		{
			name: "new json with file_input with extension txt",
			task: `
[
	{
		"name": "test json",
		"order": 1,
		"max_row_process": 0,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "json",
		"input_file": "./resource/input.txt"
	}
]`,
			errMsg: "extension 'txt' is not valid",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tasks := make([]scripts.Task, 0)
			err := json.Unmarshal([]byte(tt.task), &tasks)
			if err != nil {
				assert.Equal(t, tt.errMsg, err.Error())
			} else if tt.errMsg != "" {
				t.Fatal("error not found")
			}
		})
	}
}

func TestNewSkipColumns(t *testing.T) {
	tests := []struct {
		name   string
		task   string
		errMsg string
	}{
		{
			name: "new skip_columns success",
			task: `
[
	{
		"name": "test skip_columns",
		"order": 1,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "skip_columns"
	}
]`,
		},
		{
			name: "new skip_columns without name",
			task: `
[
	{
		"order": 1,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "skip_columns"
	}
]`,
			errMsg: "name is required",
		},
		{
			name: "new skip_columns without order",
			task: `
[
	{
		"name": "test skip_columns",
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"columns": ["1_name"],
		"type": "skip_columns"
	}
]`,
			errMsg: "order is required",
		},
		{
			name: "new skip_columns without columns",
			task: `
[
	{
		"name": "test json",
		"order": 1,
		"filters": [
			{
				"header": "person_id",
				"operation": "==",
				"value": "1"
			}
		],
		"type": "skip_columns"
	}
]`,
			errMsg: "columns is required",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tasks := make([]scripts.Task, 0)
			err := json.Unmarshal([]byte(tt.task), &tasks)
			if err != nil {
				assert.Equal(t, tt.errMsg, err.Error())
			} else if tt.errMsg != "" {
				t.Fatal("error not found")
			}
		})
	}
}
