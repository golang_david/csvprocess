package test

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
	"gitlab.com/golang_david/csvprocess/internal/core/model/scripts"
	"testing"
)

func TestExecuteCSV(t *testing.T) {
	tests := []struct {
		name           string
		task           string
		errMsg         string
		count          int
		countSuccess   int
		countErrors    int
		countFiltered  int
		headersSuccess records.Headers
	}{
		{
			name: "execute csv success",
			task: `
[
	{
		"name": "test script csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_name", "1_person_id"},
		},
		{
			name: "execute csv success with max_row_proocess",
			task: `
[
	{
		"name": "test script csv",
		"order": 1,
		"max_row_process": 2,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			count:          1,
			countSuccess:   1,
			headersSuccess: []string{"1_person_id", "1_name"},
		},
		{
			name: "execute csv when read 1 row and this is headers",
			task: `
[
	{
		"name": "test script csv",
		"order": 1,
		"max_row_process": 1,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			count:        0,
			countSuccess: 0,
		},
		{
			name: "execute csv when other delimiter",
			task: `
[
	{
		"name": "test script csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input_dotcomma.csv",
		"has_headers": true,
		"delimiter": ";"
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_person_id", "1_name"},
		},
		{
			name: "execute csv with columns to export",
			task: `
[
	{
		"name": "test script csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"columns": ["person_id"],
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_person_id"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tasks := make([]scripts.Task, 0)
			err := json.Unmarshal([]byte(tt.task), &tasks)
			if err != nil {
				t.Fatal("error unexpected: ", err.Error())
			}
			var channel <-chan records.Record
			ch := make(chan records.Record, 1)
			ch <- records.Record{}
			close(ch)
			channel = ch
			for _, task := range tasks {
				channel = task.Execute(context.Background(), channel)
			}
			count := 0
			countSuccess := 0
			countErrors := 0
			countFiltered := 0
			for record := range channel {
				count++
				switch record.Status {
				case records.ToProcess:
					countSuccess++
					assert.ElementsMatch(t, tt.headersSuccess, record.Headers)
					headersData := make([]string, 0, len(record.Data))
					for k := range record.Data {
						headersData = append(headersData, k)
					}
					assert.ElementsMatch(t, tt.headersSuccess, headersData)
				case records.WithError:
					countErrors++
				case records.Filtered:
					countFiltered++
				}
			}
			assert.Equal(t, tt.count, count)
			assert.Equal(t, tt.countSuccess, countSuccess)
			assert.Equal(t, tt.countErrors, countErrors)
			assert.Equal(t, tt.countFiltered, countFiltered)
		})
	}
}

func TestExecuteAPI(t *testing.T) {
	tests := []struct {
		name           string
		task           string
		errMsg         string
		count          int
		countSuccess   int
		countErrors    int
		countFiltered  int
		headersSuccess records.Headers
	}{
		{
			name: "api success with minimal options",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"type": "api",
		"columns": ["id"],
		"url": "http://testing.test/something",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	}
]`,
			count:          1,
			countSuccess:   1,
			headersSuccess: []string{"1_id", "1_status"},
		},
		{
			name: "api success with columns to export",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"type": "api",
		"columns": ["name", "address.street"],
		"url": "http://testing.test/something",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	}
]`,
			count:          1,
			countSuccess:   1,
			headersSuccess: []string{"1_status", "1_name", "1_address.street"},
		},
		{
			name: "api with response array and columns to export",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"type": "api",
		"columns": ["name", "address.street"],
		"url": "http://testing.test/something",
		"method": "GET",
		"mock_response": "./resource/mock_array.json"
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_status", "1_name", "1_address.street"},
		},
		{
			name: "api with root_list and columns to export",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"type": "api",
		"columns": ["parent", "name"],
		"url": "http://testing.test/something",
		"method": "GET",
		"mock_response": "./resource/mock.json",
		"root_list": "family"
	}
]`,
			count:          2,
			countSuccess:   2,
			headersSuccess: []string{"1_status", "1_parent", "1_name"},
		},
		{
			name: "api with response array and columns to export and max_row_process",
			task: `
[
	{
		"name": "test script api",
		"order": 1,
		"max_row_process": 1,
		"type": "api",
		"columns": ["name", "address.street"],
		"url": "http://testing.test/something",
		"method": "GET",
		"mock_response": "./resource/mock_array.json"
	}
]`,
			count:          1,
			countSuccess:   1,
			headersSuccess: []string{"1_status", "1_name", "1_address.street"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := context.WithValue(context.Background(), "isSimulation", true)
			tasks := make([]scripts.Task, 0)
			err := json.Unmarshal([]byte(tt.task), &tasks)
			if err != nil {
				t.Fatal("error unexpected: ", err.Error())
			}
			var channel <-chan records.Record
			ch := make(chan records.Record, 1)
			ch <- records.Record{}
			close(ch)
			channel = ch
			for _, task := range tasks {
				channel = task.Execute(ctx, channel)
			}
			count := 0
			countSuccess := 0
			countErrors := 0
			countFiltered := 0
			for record := range channel {
				count++
				switch record.Status {
				case records.ToProcess:
					countSuccess++
					assert.ElementsMatch(t, tt.headersSuccess, record.Headers)
					headersData := make([]string, 0, len(record.Data))
					for k := range record.Data {
						headersData = append(headersData, k)
					}
					assert.ElementsMatch(t, tt.headersSuccess, headersData)
				case records.WithError:
					countErrors++
				case records.Filtered:
					countFiltered++
				}
			}
			assert.Equal(t, tt.count, count)
			assert.Equal(t, tt.countSuccess, countSuccess)
			assert.Equal(t, tt.countErrors, countErrors)
			assert.Equal(t, tt.countFiltered, countFiltered)
		})
	}
}

func TestExecuteJson(t *testing.T) {
	tests := []struct {
		name           string
		task           string
		errMsg         string
		count          int
		countSuccess   int
		countErrors    int
		countFiltered  int
		headersSuccess records.Headers
	}{
		{
			name: "execute json success",
			task: `
[
	{
		"name": "test script json",
		"order": 1,
		"max_row_process": 0,
		"type": "json",
		"input_file": "./resource/input_array.json"
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_name", "1_person_id", "1_family"},
		},
		{
			name: "execute json success with max_row_proocess",
			task: `
[
	{
		"name": "test script json",
		"order": 1,
		"max_row_process": 1,
		"type": "json",
		"input_file": "./resource/input_array.json"
	}
]`,
			count:          1,
			countSuccess:   1,
			headersSuccess: []string{"1_person_id", "1_name", "1_family"},
		},
		{
			name: "execute json with columns to export",
			task: `
[
	{
		"name": "test script json",
		"order": 1,
		"max_row_process": 0,
		"type": "json",
		"columns": ["person_id"],
		"input_file": "./resource/input_array.json"
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_person_id"},
		},
		{
			name: "execute json with root_list",
			task: `
[
	{
		"name": "test script json",
		"order": 1,
		"max_row_process": 0,
		"type": "json",
		"root_list": "family",
		"columns": ["parent"],
		"input_file": "./resource/input.json"
	}
]`,
			count:          2,
			countSuccess:   2,
			headersSuccess: []string{"1_parent"},
		},
		{
			name: "execute json single",
			task: `
[
	{
		"name": "test script json",
		"order": 1,
		"max_row_process": 0,
		"type": "json",
		"input_file": "./resource/input.json"
	}
]`,
			count:          1,
			countSuccess:   1,
			headersSuccess: []string{"1_person_id", "1_name", "1_family"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tasks := make([]scripts.Task, 0)
			err := json.Unmarshal([]byte(tt.task), &tasks)
			if err != nil {
				t.Fatal("error unexpected: ", err.Error())
			}
			var channel <-chan records.Record
			ch := make(chan records.Record, 1)
			ch <- records.Record{}
			close(ch)
			channel = ch
			for _, task := range tasks {
				channel = task.Execute(context.Background(), channel)
			}
			count := 0
			countSuccess := 0
			countErrors := 0
			countFiltered := 0
			for record := range channel {
				count++
				switch record.Status {
				case records.ToProcess:
					countSuccess++
					assert.ElementsMatch(t, tt.headersSuccess, record.Headers)
					headersData := make([]string, 0, len(record.Data))
					for k := range record.Data {
						headersData = append(headersData, k)
					}
					assert.ElementsMatch(t, tt.headersSuccess, headersData)
				case records.WithError:
					countErrors++
				case records.Filtered:
					countFiltered++
				}
			}
			assert.Equal(t, tt.count, count)
			assert.Equal(t, tt.countSuccess, countSuccess)
			assert.Equal(t, tt.countErrors, countErrors)
			assert.Equal(t, tt.countFiltered, countFiltered)
		})
	}
}

func TestExecuteMix(t *testing.T) {
	tests := []struct {
		name           string
		tasks          string
		errMsg         string
		count          int
		countSuccess   int
		countErrors    int
		countFiltered  int
		headersSuccess records.Headers
	}{
		{
			name: "csv -> api-single",
			tasks: `
[
	{
		"name": "csv",
		"order": 1,
		"max_row_process": 0,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	},
	{
		"name": "api",
		"order": 2,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/{1_person_id}",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_person_id", "1_name", "2_status", "2_id", "2_name"},
		},
		{
			name: "api-single -> csv",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	},
	{
		"name": "csv",
		"order": 2,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_id", "1_name", "1_status", "2_person_id", "2_name"},
		},
		{
			name: "api-array -> csv",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock_array.json"
	},
	{
		"name": "csv",
		"order": 2,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			count:          9,
			countSuccess:   9,
			headersSuccess: []string{"1_id", "1_name", "1_status", "2_person_id", "2_name"},
		},
		{
			name: "csv -> csv",
			tasks: `
[
	{
		"name": "csv",
		"order": 1,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	},
	{
		"name": "csv",
		"order": 2,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			count:          9,
			countSuccess:   9,
			headersSuccess: []string{"1_person_id", "1_name", "2_person_id", "2_name"},
		},
		{
			name: "api-array -> api-single",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock_array.json"
	},
	{
		"name": "api",
		"order": 2,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/{1_id}",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_status", "1_id", "1_name", "2_status", "2_id", "2_name"},
		},
		{
			name: "api-single -> api-array",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	},
	{
		"name": "api",
		"order": 2,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/{1_id}",
		"method": "GET",
		"mock_response": "./resource/mock_array.json"
	}
]`,
			count:          3,
			countSuccess:   3,
			headersSuccess: []string{"1_status", "1_id", "1_name", "2_status", "2_id", "2_name"},
		},
		{
			name: "api-array -> api-array",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock_array.json"
	},
	{
		"name": "api",
		"order": 2,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/{1_id}",
		"method": "GET",
		"mock_response": "./resource/mock_array.json"
	}
]`,
			count:          9,
			countSuccess:   9,
			headersSuccess: []string{"1_status", "1_id", "1_name", "2_status", "2_id", "2_name"},
		},
		{
			name: "csv -> api-array",
			tasks: `
[
	{
		"name": "csv",
		"order": 1,
		"type": "csv",
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	},
	{
		"name": "api",
		"order": 2,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock_array.json"
	}
]`,
			count:          9,
			countSuccess:   9,
			headersSuccess: []string{"1_person_id", "1_name", "2_status", "2_id", "2_name"},
		},
		{
			name: "api-single -> api-single",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	},
	{
		"name": "api",
		"order": 2,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	}
]`,
			count:          1,
			countSuccess:   1,
			headersSuccess: []string{"1_status", "1_id", "1_name", "2_status", "2_id", "2_name"},
		},
		{
			name: "api-single with root_list -> api-single",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["parent", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json",
		"root_list": "family"
	},
	{
		"name": "api",
		"order": 2,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	}
]`,
			count:          2,
			countSuccess:   2,
			headersSuccess: []string{"1_status", "1_parent", "1_name", "2_status", "2_id", "2_name"},
		},
		{
			name: "api-array with root_list failure",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock_array.json",
		"root_list": "family"
	}
]`,
			count:          1,
			countErrors:    1,
			headersSuccess: []string{"1_status", "1_errors"},
		},
		{
			name: "api-single -> api-single with root_list",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json"
	},
	{
		"name": "api",
		"order": 2,
		"type": "api",
		"columns": ["parent", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json",
		"root_list": "family"
	}
]`,
			count:          2,
			countSuccess:   2,
			headersSuccess: []string{"1_status", "1_id", "1_name", "2_status", "2_parent", "2_name"},
		},
		{
			name: "api-single with root_list -> api-single with root_list",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["parent", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json",
		"root_list": "family"
	},
	{
		"name": "api",
		"order": 2,
		"type": "api",
		"columns": ["parent", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock.json",
		"root_list": "family"
	}
]`,
			count:          4,
			countSuccess:   4,
			headersSuccess: []string{"1_status", "1_parent", "1_name", "2_status", "2_parent", "2_name"},
		},
		{
			name: "api-single -> csv",
			tasks: `
[
	{
		"name": "api",
		"order": 1,
		"type": "api",
		"columns": ["id", "name"],
		"url": "http://testing.test/person/",
		"method": "GET",
		"mock_response": "./resource/mock_array.json"
	},
	{
		"name": "csv",
		"order": 2,
		"max_row_process": 0,
		"type": "csv",
		"filters": [
			{
				"header": "1_id",
				"operation": "==",
				"value": "1"
			}
		],
		"input_file": "./resource/input.csv",
		"has_headers": true,
		"delimiter": ","
	}
]`,
			count:          5,
			countSuccess:   3,
			countFiltered:  2,
			headersSuccess: []string{"1_id", "1_name", "1_status", "2_person_id", "2_name"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := context.WithValue(context.Background(), "isSimulation", true)
			tasks := make([]scripts.Task, 0)
			err := json.Unmarshal([]byte(tt.tasks), &tasks)
			if err != nil {
				t.Fatal("error unexpected: ", err.Error())
			}
			var channel <-chan records.Record
			ch := make(chan records.Record, 1)
			ch <- records.Record{}
			close(ch)
			channel = ch
			for _, task := range tasks {
				channel = task.Execute(ctx, channel)
			}
			count := 0
			countSuccess := 0
			countErrors := 0
			countFiltered := 0
			for record := range channel {
				fmt.Println(record)
				count++
				switch record.Status {
				case records.ToProcess:
					countSuccess++
					assert.ElementsMatch(t, tt.headersSuccess, record.Headers)
					headersData := make([]string, 0, len(record.Data))
					for k := range record.Data {
						headersData = append(headersData, k)
					}
					assert.ElementsMatch(t, tt.headersSuccess, headersData)
				case records.WithError:
					countErrors++
				case records.Filtered:
					countFiltered++
				}
			}
			assert.Equal(t, tt.count, count)
			assert.Equal(t, tt.countSuccess, countSuccess)
			assert.Equal(t, tt.countErrors, countErrors)
			assert.Equal(t, tt.countFiltered, countFiltered)
		})
	}
}
