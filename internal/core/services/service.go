package services

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
	"gitlab.com/golang_david/csvprocess/internal/core/model/scripts"
	"gitlab.com/golang_david/csvprocess/internal/core/model/summary"
	"gitlab.com/golang_david/csvprocess/internal/core/services/prompt"
	"gitlab.com/golang_david/csvprocess/internal/ports"
	"gitlab.com/golang_david/csvprocess/internal/values"
	"gitlab.com/golang_david/csvprocess/pkg/wfiles"
	"gitlab.com/golang_david/csvprocess/pkg/wurub"
	"log"
	"runtime"
	"time"
)

type Service struct{}

func NewService() ports.Service {
	return &Service{}
}

func (s *Service) Run(isSimulation bool, withTime bool) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctx = context.WithValue(ctx, values.KeyIsSimulation, isSimulation)
	ctx = context.WithValue(ctx, values.KeyWithSleep, withTime)

	// generate outputs resources
	generateOutputs()

	// select environment if exist
	varEnvironments := loadEnvironments()

	// Load Tasks
	tasks := loadTasks()

	// Confirm execution
	confirm, err := wurub.SelectOptions([]string{"No", "Yes"}, "Confirm Process Script??")
	if err != nil || confirm != "Yes" {
		return nil
	}

	summary.SetStartTime(time.Now())
	// Print summary on file log and console when finished
	defer func() {
		s := summary.GetSummary()
		fmt.Println(s)
		log.Println(s)
	}()
	// Print status every second
	go printStatusProcess(ctx)

	firstChan := make(chan records.Record, 1)
	record := records.NewRecords(0)
	record.Data = varEnvironments
	firstChan <- record
	var channel <-chan records.Record = firstChan
	for _, task := range tasks {
		channel = task.Execute(ctx, channel)
	}
	close(firstChan)

	CatchResults(ctx, channel)

	fmt.Printf("\nview file -> %s\n", summary.GetFilenameLog())
	return nil
}

func generateOutputs() {
	// generate output directory
	outputDir := GenerateOutputDirectory()
	summary.SetOutputDirectory(outputDir)
	summary.SetFilenameLog(SetLogFile(outputDir))
	//filenameOriginal := directories.GetOriginalInput(ctx, outputDir)
	summary.SetFilenameSuccessful(GetSuccessResultFileName(outputDir))
	summary.SetFilenameFailed(GetErrorResultFileName(outputDir))
	summary.SetFilenameFiltered(GetFilterResultFileName(outputDir))
}

func loadEnvironments() map[string]string {
	data := map[string]string{}
	defer func(data map[string]string) {
		now := time.Now()
		data["__date__"] = now.Format("2006-01-02")
		data["__datetime__"] = now.Format("2006-01-02T15:04:05Z")
	}(data)
	envFilename := prompt.GetEnvironment()
	if envFilename != nil {
		content, err := wfiles.ReadAll(*envFilename)
		if err != nil {
			log.Printf("Warning: loadEnvironments | ReadFileConfig | %s\n", err.Error())
			return nil
		}
		err = json.Unmarshal(content, &data)
		if err != nil {
			log.Printf("Warning: loadEnvironments | Unmarshal | %s\n", err.Error())
			return nil
		}
		return data
	}
	return data
}

func loadTasks() []scripts.Task {
	// select configuration directory
	configFilename, err := prompt.GetFileConfig()
	if err != nil {
		log.Fatal("Error GetFileConfig | ", err)
	}
	summary.SetConfigSelected(configFilename)

	// Read File configuration
	content, err := wfiles.ReadAll(configFilename)
	if err != nil {
		log.Fatal("Error ReadFileConfig | ", err)
	}

	list := make([]map[string]interface{}, 0, 0)
	err = json.Unmarshal(content, &list)
	if err != nil {
		log.Fatal("Error UnmarshalTasks | ", err)
	}
	// Unmarshall Tasks
	tasks := make([]scripts.Task, 0, len(list))
	for _, m := range list {
		if val, ok := m["enable"]; ok {
			if v, ok := val.(bool); ok {
				if !v {
					continue
				}
			} else {
				log.Fatal("Error to read Enable | ", fmt.Sprintf("enable=%v (%T)", val, val))
			}
		}
		b, err := json.Marshal(m)
		task := scripts.Task{}
		err = json.Unmarshal(b, &task)
		if err != nil {
			log.Fatal("Error UnmarshalTasks | ", err)
		}
		tasks = append(tasks, task)
	}
	return tasks
}

func printStatusProcess(_ context.Context) {
	lastProcess := 0
	for {
		time.Sleep(time.Duration(1) * time.Second)
		routines := runtime.NumGoroutine()
		fmt.Printf("\nGoRoutines Alive: %d\tProcess Rows: %d\tRPS:%d",
			routines, ProcessRows, ProcessRows-lastProcess)
		lastProcess = ProcessRows
	}
}
