package services

import (
	"context"
	"encoding/csv"
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
	"gitlab.com/golang_david/csvprocess/internal/core/model/summary"
	"gitlab.com/golang_david/csvprocess/pkg/wfiles"
	"gitlab.com/golang_david/csvprocess/pkg/wurub"
	"log"
	"os"
	"sync"
)

var (
	SuccessRows  = 0
	ErrorsRows   = 0
	FilteredRows = 0
	ProcessRows  = 0
	writers      = make(map[string]*csv.Writer)
	files        = make(map[string]*os.File)
)

func CatchResults(ctx context.Context, inputs <-chan records.Record) {
	wg := &sync.WaitGroup{}
	wg.Add(3)
	success := catchSuccess(ctx, wg, summary.GetFilenameSuccessful())
	withErrors := catchErrors(ctx, wg, summary.GetFilenameFailed())
	filtered := catchFiltered(ctx, wg, summary.GetFilenameFiltered())
	for record := range inputs {
		switch record.Status {
		case records.ToProcess:
			success <- record
		case records.WithError:
			withErrors <- record
		case records.Filtered:
			filtered <- record
		default:
			log.Println("Error records with status unknown")
		}
	}
	close(success)
	close(withErrors)
	close(filtered)
	wg.Wait()
	closeResources()
	summary.SetRowsProcess(ProcessRows)
	summary.SetRowsSuccess(SuccessRows)
	summary.SetRowsErrors(ErrorsRows)
	summary.SetRowsFiltered(FilteredRows)
}

func closeResources() {
	for _, v := range writers {
		v.Flush()
	}
	for _, v := range files {
		wfiles.CloseResource(v)
	}
}

func catchSuccess(_ context.Context, wg *sync.WaitGroup, filename string) chan<- records.Record {
	inputs := make(chan records.Record)
	go func() {
		SuccessRows = catch(filename, inputs)
		wg.Done()
	}()
	return inputs
}

func catchErrors(_ context.Context, wg *sync.WaitGroup, filename string) chan<- records.Record {
	inputs := make(chan records.Record)
	go func() {
		ErrorsRows = catch(filename, inputs)
		wg.Done()
	}()
	return inputs
}

func catchFiltered(_ context.Context, wg *sync.WaitGroup, filename string) chan<- records.Record {
	inputs := make(chan records.Record)
	go func() {
		FilteredRows = catch(filename, inputs)
		wg.Done()
	}()
	return inputs
}

func catch(filename string, channel <-chan records.Record) int {
	count := 0
	for record := range channel {
		writer, err := getWriter(fmt.Sprintf(filename, record.LastScript), record.Headers)
		if err != nil {
			log.Fatal("Error to getWriter:", err)
		}
		ProcessRows++
		count++
		func() {
			defer wurub.RecoveryFunc()
			var data []string
			for _, v := range record.Headers {
				data = append(data, record.Data[v])
			}
			err := writer.Write(data)
			if err != nil {
				log.Fatal("Error to write data: ", err)
			}
		}()
	}
	return count
}

func getWriter(filename string, headers []string) (*csv.Writer, error) {
	if writer, ok := writers[filename]; ok {
		return writer, nil
	}
	file, err := wfiles.CreateOrOpenFile(filename)
	if err != nil {
		return nil, err
	}
	files[filename] = file
	writer := csv.NewWriter(file)
	err = writer.Write(headers)
	if err != nil {
		log.Fatal("Error to write headers: ", err)
	}
	writers[filename] = writer
	return writer, nil
}
