package prompt

import (
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/core/model/summary"
	"gitlab.com/golang_david/csvprocess/internal/values"
	"gitlab.com/golang_david/csvprocess/pkg/wurub"
	"log"
)

func GetFileConfig() (string, error) {
	dirConfig, err := wurub.SelectDirectory(values.ConfigDirectory, "Select what config you wish to run")
	if err != nil {
		return "", err
	}
	filename := fmt.Sprintf("%s/%s/%s", values.ConfigDirectory, dirConfig, values.ConfigFilename)
	return filename, nil
}

func GetEnvironment() *string {
	filename, err := wurub.SelectFile(values.EnvironmentDirectory, "Select environment to use:")
	if err != nil {
		log.Printf("Warning: Environment not load | %s\n", err.Error())
		return nil
	}
	if filename == "" {
		log.Println("Warning: Environment not load | filename is empty")
		return nil
	}
	inputFile := fmt.Sprintf("%s/%s", values.EnvironmentDirectory, filename)
	summary.SetInputSelected(inputFile)
	return &inputFile
}

func GetInputFile(directory string, name string) (string, error) {
	filename, err := wurub.SelectFile(directory, fmt.Sprintf("Select what file you wish to process for %s", name))
	if err != nil {
		return "", err
	}
	inputFile := fmt.Sprintf("%s/%s", directory, filename)
	summary.SetInputSelected(inputFile)
	return inputFile, nil
}
