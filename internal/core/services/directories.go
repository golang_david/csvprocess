package services

import (
	"context"
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/values"
	"log"
	"os"
	"time"
)

func GenerateOutputDirectory() string {
	markTime := time.Now().Format("20060102150405")
	outputDir := fmt.Sprintf("%s/%s", values.OutputDirectory, markTime)

	if _, err := os.Stat(values.OutputDirectory); os.IsNotExist(err) {
		if err = os.Mkdir(values.OutputDirectory, os.ModePerm); err != nil {
			panic(fmt.Sprintf("Error: GenerateOutputDirectory | %s", err.Error()))
		}
	}
	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		if err = os.Mkdir(outputDir, os.ModePerm); err != nil {
			panic(fmt.Sprintf("Error: GenerateOutputDirectory | %s", err.Error()))
		}
	}
	return outputDir
}

func SetLogFile(directory string) string {
	logFilename := fmt.Sprintf("%s/%s", directory, values.LogFilename)
	fileLog, err := os.OpenFile(logFilename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	log.SetOutput(fileLog)
	return logFilename
}

func GetOriginalInput(_ context.Context, directory string) string {
	filename := fmt.Sprintf("%s/%s", directory, values.CopyInputFilename)
	return filename
}

func GetSuccessResultFileName(directory string) string {
	filename := fmt.Sprintf("%s/%s", directory, values.SuccessResultFilename)
	return filename
}

func GetErrorResultFileName(directory string) string {
	filename := fmt.Sprintf("%s/%s", directory, values.ErrorResultFilename)
	return filename
}

func GetFilterResultFileName(directory string) string {
	filename := fmt.Sprintf("%s/%s", directory, values.FilterResultFilename)
	return filename
}
