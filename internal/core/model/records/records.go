package records

import (
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/values"
	"sync"
)

var (
	mu = sync.Mutex{}
)

type StatusRecord int

const (
	ToProcess StatusRecord = iota
	WithError
	Filtered
)

type Headers []string
type Data map[string]string

type Record struct {
	Headers    Headers
	Data       Data
	Status     StatusRecord
	LastScript int
}

func (r *Record) SetError(msg string, lastScript int) {
	mu.Lock()
	defer mu.Unlock()
	key := fmt.Sprintf("%d_%s", lastScript, values.KeyErrors)
	r.Headers = append(r.Headers, key)
	r.Data[key] = msg
	r.Status = WithError
}

func (r *Record) MarkFiltered(lastScript int) {
	mu.Lock()
	defer mu.Unlock()
	r.Status = Filtered
	r.LastScript = lastScript
}

func (r *Record) SetStatusCode(statusCode int, lastScript int) {
	mu.Lock()
	defer mu.Unlock()
	key := fmt.Sprintf("%d_%s", lastScript, values.KeyStatus)
	r.Headers = append(r.Headers, key)
	r.Data[key] = fmt.Sprintf("%d", statusCode)
}

func NewRecords(lastScript int) Record {
	return Record{
		Headers:    []string{},
		Data:       map[string]string{},
		Status:     ToProcess,
		LastScript: lastScript,
	}
}

func (r *Record) Copy() Record {
	record := NewRecords(r.LastScript)
	record.Headers = append(record.Headers, r.Headers...)
	record.Status = r.Status
	record.LastScript = r.LastScript
	for k, v := range r.Data {
		record.Data[k] = v
	}
	return record
}
