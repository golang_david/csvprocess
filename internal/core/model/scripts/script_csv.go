package scripts

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/core/model/filter"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
	"gitlab.com/golang_david/csvprocess/internal/core/services/prompt"
	"gitlab.com/golang_david/csvprocess/pkg/wfiles"
	"gitlab.com/golang_david/csvprocess/pkg/wpatterns"
	"gitlab.com/golang_david/csvprocess/pkg/wstrings"
	"gitlab.com/golang_david/csvprocess/pkg/wurub"
	"io"
	"log"
	"path"
	"regexp"
	"strings"
)

func init() {
	Register("csv", newScriptCSV)
}

type scriptCSV struct {
	Name             string          `json:"name"`
	Order            int             `json:"order"`
	MaxRowProcess    int             `json:"max_row_process"`
	Filters          []filter.Filter `json:"filters"`
	Columns          []string        `json:"columns"`
	Workers          int             `json:"workers"`
	InputFile        string          `json:"input_file"`
	InputDirectory   string          `json:"input_directory"`
	HasHeaders       bool            `json:"has_headers"`
	Delimiter        string          `json:"delimiter"`
	ColumnsNotExport []string        `json:"columns_not_export"`
}

func newScriptCSV(data []byte) (Script, error) {
	script := scriptCSV{}
	err := json.Unmarshal(data, &script)
	if err != nil {
		return nil, err
	}
	if script.Workers == 0 {
		script.Workers = defaultWorkers
	}
	// Get Input File
	if script.InputFile == "" && script.InputDirectory != "" {
		inputFilename, err := prompt.GetInputFile(script.InputDirectory, script.Name)
		if err != nil {
			return nil, fmt.Errorf("script_csv: %s", err.Error())
		}
		script.InputFile = inputFilename
	}
	err = script.validations()
	if err != nil {
		return nil, err
	}
	return &script, nil
}

func (s *scriptCSV) Execute(ctx context.Context, inputs <-chan records.Record) <-chan records.Record {
	if inputs == nil {
		panic("scriptCSV -> input is nil")
	}
	listOutputs := make([]<-chan records.Record, 0)
	for i := 0; i < s.Workers; i++ {
		output := make(chan records.Record)
		listOutputs = append(listOutputs, output)
		go func(output chan records.Record) {
			defer wurub.RecoveryFunc()
			defer close(output)
			for input := range inputs {
				s.readAndEnqueueData(&input, s.MaxRowProcess, output, s.Filters, s.Columns)
			}
		}(output)
	}
	return fanIn(listOutputs...)
}

func (s *scriptCSV) readAndEnqueueData(r *records.Record, maxRowProcess int, output chan records.Record, filters []filter.Filter, columns []string) {

	for i := range s.Filters {
		header, err := wpatterns.ReplacePatternInString(s.Filters[i].Header, r.Data)
		if err != nil {
			msg := "script_api: cannot buildFilters Header | " + err.Error()
			log.Println(msg)
			r.SetError(msg, s.Order)
			output <- *r
			return
		}
		val, err := wpatterns.ReplacePatternInString(s.Filters[i].Value, r.Data)
		if err != nil {
			msg := "script_api: cannot buildFilters Value | " + err.Error()
			log.Println(msg)
			r.SetError(msg, s.Order)
			output <- *r
			return
		}
		if filter.CheckData(r.Data, header, s.Filters[i].Operation, val) {
			r.MarkFiltered(s.Order)
			output <- *r
			return
		}
	}

	csvReader, readerClose, err := getCsvReader(s.InputFile, s.Delimiter)
	if err != nil {
		log.Fatal(err)
	}
	defer wfiles.CloseResource(readerClose)

	var headers records.Headers
	row := 0
	for {
		record := records.NewRecords(s.Order)
		if r != nil {
			record = r.Copy()
		}
		if maxRowProcess > 0 && row >= maxRowProcess {
			break
		}
		// Read first row
		recordRow, err := csvReader.Read()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal("script_csv: cannot read CSV | ", err.Error())
			}
		}
		row++
		normalizeRecord(recordRow)
		if row == 1 {
			if s.HasHeaders {
				headers = recordRow
				validateHeaders(headers)
				continue
			} else {
				// implicit headers
				for i := 0; i < len(recordRow); i++ {
					headers = append(headers, fmt.Sprintf("X%d", i))
				}
			}
		}

		// build data
		for i := 0; i < len(headers); i++ {
			if contained(headers[i], columns) {
				key := fmt.Sprintf("%d_%s", s.Order, headers[i])
				record.Headers = append(record.Headers, key)
				record.Data[key] = recordRow[i]
			}
		}
		record.LastScript = s.Order
		record.Status = records.ToProcess
		output <- record
	}
}

func (s *scriptCSV) filterData(_ records.Record) bool {
	return false
}

func getCsvReader(filename string, delimiter string) (*csv.Reader, io.ReadCloser, error) {
	file, err := wfiles.OpenFile(filename)
	if err != nil {
		return nil, nil, fmt.Errorf("script_csv: cannot open file | %s ", err.Error())
	}
	csvReader := csv.NewReader(file)
	sliceRune := []rune(delimiter)
	if len(sliceRune) != 1 {
		return nil, nil, fmt.Errorf("delimiter (%s) is invalid", delimiter)
	}
	csvReader.Comma = sliceRune[0]
	return csvReader, file, nil
}

func normalizeRecord(records []string) {
	for i := range records {
		records[i] = wstrings.NormalizeString(records[i])
	}
}

func validateHeaders(headers records.Headers) {
	pattern := "[0-9]+_"
	reg, err := regexp.Compile(pattern)
	if err != nil {
		log.Fatal("Error when compile regexp to validate headers: ", err)
	}
	for _, h := range headers {
		if strings.Contains(h, " ") {
			log.Fatal(fmt.Sprintf("Error with header '%s', must not contain space blank", h))
		}
		if reg.MatchString(h) {
			log.Fatal(fmt.Sprintf("Error with header '%s', must not start with '[0-9]_'", h))
		}
	}
}

func contained(val string, list []string) bool {
	if len(list) == 0 {
		return true
	}
	for _, s := range list {
		if s == val {
			return true
		}
	}
	return false
}

func (s *scriptCSV) validations() error {
	if s.Name == "" {
		return errors.New("name is required")
	}
	if s.Order == 0 {
		return errors.New("order is required")
	}
	if s.Delimiter == "" {
		return errors.New("delimiter is required")
	}
	err := checkFormatCsv(s.InputFile)
	if err != nil {
		return err
	}
	err = checkDelimiter(s.Delimiter)
	if err != nil {
		return err
	}
	return nil
}

func checkFormatCsv(filename string) error {
	if filename == "" {
		return errors.New("file is required")
	}
	ext := path.Ext(filename)
	ext = strings.Trim(ext, ".")
	if ext == "" {
		return errors.New("file without extension")
	}
	if strings.ToUpper("csv") != strings.ToUpper(ext) {
		return fmt.Errorf("extension '%s' is not valid", ext)
	}
	return nil
}

func checkDelimiter(delimiter string) error {
	if delimiter == "." ||
		delimiter == "\n" ||
		delimiter == "\r" ||
		delimiter == "\n\r" {
		return fmt.Errorf("delimiter (%s) is not supported", delimiter)
	}
	return nil
}
