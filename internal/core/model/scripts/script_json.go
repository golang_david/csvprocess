package scripts

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/core/model/filter"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
	"gitlab.com/golang_david/csvprocess/internal/core/services/prompt"
	"gitlab.com/golang_david/csvprocess/pkg/wpatterns"
	"gitlab.com/golang_david/csvprocess/pkg/wurub"
	"io/ioutil"
	"log"
	"path"
	"strings"
)

func init() {
	Register("json", newScriptJSON)
}

type scriptJSON struct {
	Name           string          `json:"name"`
	Order          int             `json:"order"`
	MaxRowProcess  int             `json:"max_row_process"`
	Filters        []filter.Filter `json:"filters"`
	Columns        []string        `json:"columns"`
	Workers        int             `json:"workers"`
	InputFile      string          `json:"input_file"`
	InputDirectory string          `json:"input_directory"`
	RootList       string          `json:"root_list"`
}

func newScriptJSON(data []byte) (Script, error) {
	script := scriptJSON{}
	err := json.Unmarshal(data, &script)
	if err != nil {
		return nil, err
	}
	if script.Workers == 0 {
		script.Workers = defaultWorkers
	}
	// Get Input File
	if script.InputFile == "" && script.InputDirectory != "" {
		inputFilename, err := prompt.GetInputFile(script.InputDirectory, script.Name)
		if err != nil {
			return nil, fmt.Errorf("script_json: %s", err.Error())
		}
		script.InputFile = inputFilename
	}
	err = script.validations()
	if err != nil {
		return nil, err
	}
	return &script, nil
}

func (s *scriptJSON) Execute(ctx context.Context, inputs <-chan records.Record) <-chan records.Record {
	if inputs == nil {
		panic("scriptJSON -> input is nil")
	}
	listOutputs := make([]<-chan records.Record, 0)
	for i := 0; i < s.Workers; i++ {
		output := make(chan records.Record)
		listOutputs = append(listOutputs, output)
		go func(output chan records.Record) {
			defer wurub.RecoveryFunc()
			defer close(output)
			for input := range inputs {
				s.readAndEnqueueData(&input, output)
			}
		}(output)
	}
	return fanIn(listOutputs...)
}

func (s *scriptJSON) readAndEnqueueData(r *records.Record, output chan records.Record) {
	record := records.NewRecords(s.Order)
	if r != nil {
		record = r.Copy()
	}
	if record.Status != records.ToProcess {
		output <- record
		return
	}
	for i := range s.Filters {
		header, err := wpatterns.ReplacePatternInString(s.Filters[i].Header, record.Data)
		if err != nil {
			msg := "script_api: cannot buildFilters Header | " + err.Error()
			log.Println(msg)
			record.SetError(msg, s.Order)
			output <- record
			return
		}
		val, err := wpatterns.ReplacePatternInString(s.Filters[i].Value, record.Data)
		if err != nil {
			msg := "script_api: cannot buildFilters Value | " + err.Error()
			log.Println(msg)
			record.SetError(msg, s.Order)
			output <- record
			return
		}
		if filter.CheckData(record.Data, header, s.Filters[i].Operation, val) {
			record.MarkFiltered(s.Order)
			output <- record
			return
		}
	}

	content, err := ioutil.ReadFile(s.InputFile)
	if err != nil {
		log.Fatal(err)
	}
	var data interface{}
	err = json.Unmarshal(content, &data)
	if err != nil {
		log.Fatal(err)
	}

	list := addColumnResponseToRecord(record, s.RootList, s.Order, data, s.Columns)
	count := 0
	for _, r := range list {
		count++
		if s.MaxRowProcess > 0 && count > s.MaxRowProcess {
			return
		}
		r.LastScript = s.Order
		output <- r
	}
}

func (s *scriptJSON) validations() error {
	if s.Name == "" {
		return errors.New("name is required")
	}
	if s.Order == 0 {
		return errors.New("order is required")
	}
	err := checkFormatJson(s.InputFile)
	if err != nil {
		return err
	}
	return nil
}

func checkFormatJson(filename string) error {
	if filename == "" {
		return errors.New("file is required")
	}
	ext := path.Ext(filename)
	ext = strings.Trim(ext, ".")
	if ext == "" {
		return errors.New("file without extension")
	}
	if strings.ToUpper("json") != strings.ToUpper(ext) {
		return fmt.Errorf("extension '%s' is not valid", ext)
	}
	return nil
}
