package scripts

import (
	"context"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
)

type Script interface {
	Execute(ctx context.Context, input <-chan records.Record) <-chan records.Record
}
