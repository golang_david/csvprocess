package scripts

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
	"sync"
)

var (
	mu          sync.Mutex
	listScripts = make(map[string]Factory)
)

func Register(taskType string, f Factory) {
	mu.Lock()
	defer mu.Unlock()
	if f == nil {
		panic("task: Register task is nil")
	}
	if _, ok := listScripts[taskType]; ok {
		panic("task: Register called twice for task " + taskType)
	}
	listScripts[taskType] = f
}

type Factory func(data []byte) (Script, error)
type Task struct {
	TaskImpl Script
}

func (t *Task) UnmarshalJSON(data []byte) error {
	dto := map[string]interface{}{}
	err := json.Unmarshal(data, &dto)
	if err != nil {
		return err
	}
	if val, ok := dto["type"]; ok {
		typeScript, okCast := val.(string)
		if !okCast {
			return fmt.Errorf("unknown format type %T", val)
		}
		mu.Lock()
		factory, exist := listScripts[typeScript]
		mu.Unlock()
		if !exist {
			return fmt.Errorf("unknown type %s", typeScript)
		}
		script, err := factory(data)
		if err != nil {
			return err
		}
		t.TaskImpl = script
	} else {
		return errors.New("missing type")
	}
	return nil
}

func (t *Task) Execute(ctx context.Context, input <-chan records.Record) <-chan records.Record {
	output := t.TaskImpl.Execute(ctx, input)
	return output
}
