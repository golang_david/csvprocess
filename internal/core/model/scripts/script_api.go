package scripts

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/core/model/filter"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
	"gitlab.com/golang_david/csvprocess/internal/core/services/prompt"
	"gitlab.com/golang_david/csvprocess/pkg/wfiles"
	"gitlab.com/golang_david/csvprocess/pkg/wpatterns"
	"gitlab.com/golang_david/csvprocess/pkg/wurub"
	"io/ioutil"
	"log"
	"runtime"
	"sync"
)

func init() {
	Register("api", newScriptAPI)
}

var (
	defaultWorkers = runtime.NumCPU() * 4
)

type scriptAPI struct {
	Name              string                   `json:"name"`
	Order             int                      `json:"order"`
	MaxRowProcess     int                      `json:"max_row_process"`
	Filters           []filter.Filter          `json:"filters"`
	Columns           []string                 `json:"columns"`
	Workers           int                      `json:"workers"`
	Url               string                   `json:"url"`
	Method            string                   `json:"method"`
	Headers           map[string]string        `json:"headers"`
	Params            map[string]string        `json:"params"`
	BodyTemplate      string                   `json:"body_template"`
	BodyDirectory     string                   `json:"body_directory"`
	MockResponse      string                   `json:"mock_response"`
	RootList          string                   `json:"root_list"`
	RestClientConfig  wurub.RestClientConfig   `json:"rest_client_config"`
	RateLimiterConfig *wurub.RateLimiterConfig `json:"rate_limiter_config"`
	ColumnsNotExport  []string                 `json:"columns_not_export"`
	body              []byte
	mock              []byte
	restClient        wurub.RestClient
}

func newScriptAPI(data []byte) (Script, error) {
	script := scriptAPI{}
	err := json.Unmarshal(data, &script)
	if err != nil {
		return nil, err
	}
	if script.Workers == 0 {
		script.Workers = defaultWorkers
	}
	if script.BodyTemplate == "" && script.BodyDirectory != "" {
		inputFilename, err := prompt.GetInputFile(script.BodyDirectory, script.Name)
		if err != nil {
			return nil, fmt.Errorf("script_csv: %s", err.Error())
		}
		script.BodyTemplate = inputFilename
	}
	if script.BodyTemplate != "" {
		bytes, err := wfiles.ReadAll(script.BodyTemplate)
		if err != nil {
			return nil, fmt.Errorf("failed read %s | %s", script.BodyTemplate, err.Error())
		}
		script.body = bytes
	}
	if script.MockResponse != "" {
		bytes, err := wfiles.ReadAll(script.MockResponse)
		if err != nil {
			return nil, fmt.Errorf("failed read %s | %s", script.MockResponse, err.Error())
		}
		script.mock = bytes
	}
	options := make([]wurub.RestClientOptions, 0)
	if script.RateLimiterConfig != nil {
		options = append(options, wurub.WithRateLimiter(wurub.NewRateLimiter(*script.RateLimiterConfig)))
	}
	script.restClient = wurub.NewRestClient(script.RestClientConfig, options...)
	err = script.Validations()
	if err != nil {
		return nil, err
	}
	return &script, nil
}

func (s *scriptAPI) Execute(ctx context.Context, input <-chan records.Record) <-chan records.Record {
	if input == nil {
		panic("scriptAPI -> input is nil")
	}
	listOutputs := make([]<-chan records.Record, 0)
	for i := 0; i < s.Workers; i++ {
		output := make(chan records.Record)
		listOutputs = append(listOutputs, output)
		go func(output chan records.Record) {
			defer wurub.RecoveryFunc()
			defer close(output)
			s.fetchWithInput(ctx, input, output)
		}(output)
	}
	return fanIn(listOutputs...)
}

func fanIn(chans ...<-chan records.Record) <-chan records.Record {
	out := make(chan records.Record)
	wg := &sync.WaitGroup{}
	wg.Add(len(chans))
	for _, c := range chans {
		go func(records <-chan records.Record) {
			for r := range records {
				out <- r
			}
			wg.Done()
		}(c)
	}
	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

func (s *scriptAPI) fetchWithInput(ctx context.Context, input <-chan records.Record, output chan<- records.Record) {
	for record := range input {
		s.fetch(ctx, &record, output)
	}
}

func (s *scriptAPI) fetch(ctx context.Context, r *records.Record, output chan<- records.Record) {
	record := records.NewRecords(s.Order)
	if r != nil {
		record = r.Copy()
	}
	if record.Status != records.ToProcess {
		output <- record
		return
	}
	for i := range s.Filters {
		header, err := wpatterns.ReplacePatternInString(s.Filters[i].Header, record.Data)
		if err != nil {
			msg := "script_api: cannot buildFilters Header | " + err.Error()
			log.Println(msg)
			record.SetError(msg, s.Order)
			output <- record
			return
		}
		val, err := wpatterns.ReplacePatternInString(s.Filters[i].Value, record.Data)
		if err != nil {
			msg := "script_api: cannot buildFilters Value | " + err.Error()
			log.Println(msg)
			record.SetError(msg, s.Order)
			output <- record
			return
		}
		if filter.CheckData(record.Data, header, s.Filters[i].Operation, val) {
			record.MarkFiltered(s.Order)
			output <- record
			return
		}
	}
	url, err := wpatterns.ReplacePatternInString(s.Url, record.Data)
	if err != nil {
		msg := "script_api: cannot buildURL | " + err.Error()
		log.Println(msg)
		record.SetError(msg, s.Order)
		output <- record
		return
	}
	params, err := wpatterns.ReplacePatternInMap(s.Params, record.Data)
	if err != nil {
		msg := "script_api: cannot buildParams | " + err.Error()
		log.Println(msg)
		record.SetError(msg, s.Order)
		output <- record
		return
	}
	headers, err := wpatterns.ReplacePatternInMap(s.Headers, record.Data)
	if err != nil {
		msg := "script_api: cannot buildHeaders | " + err.Error()
		log.Println(msg)
		record.SetError(msg, s.Order)
		output <- record
		return
	}
	body, err := wpatterns.ReplacePatternInBytes(s.body, record.Data)
	if err != nil {
		msg := "script_api: cannot buildBody | " + err.Error()
		log.Println(msg)
		record.SetError(msg, s.Order)
		output <- record
		return
	}
	mockResponse, err := wpatterns.ReplacePatternInBytes(s.mock, record.Data)
	if err != nil {
		msg := "script_api: cannot buildMockResponse | " + err.Error()
		log.Println(msg)
		record.SetError(msg, s.Order)
		output <- record
		return
	}
	resp, err := s.restClient.Do(ctx, s.Method, url, params, headers, body, mockResponse)
	if err != nil {
		record.SetError(err.Error(), s.Order)
		output <- record
		return
	}
	record.SetStatusCode(resp.StatusCode, s.Order)
	bodyResponse, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		msg := fmt.Sprintf("script_api: cannot read body response: %s", err.Error())
		record.SetError(msg, s.Order)
		output <- record
		return
	}
	if resp.StatusCode >= 300 {
		record.SetError(string(bodyResponse), s.Order)
		output <- record
		return
	}
	var bodyParsed interface{}
	err = json.Unmarshal(bodyResponse, &bodyParsed)
	if err != nil {
		record.SetError(err.Error(), s.Order)
		output <- record
		return
	}
	list := addColumnResponseToRecord(record, s.RootList, s.Order, bodyParsed, s.Columns)
	count := 0
	for _, r := range list {
		count++
		if s.MaxRowProcess > 0 && count > s.MaxRowProcess {
			return
		}
		r.LastScript = s.Order
		output <- r
	}
}

func addColumnResponseToRecord(record records.Record, rootList string, order int, data interface{}, colsResp []string) []records.Record {
	list := make([]records.Record, 0)
	r := wpatterns.FindResponseV3(rootList, data)
	switch r.(type) {
	case []interface{}:
		if len(r.([]interface{})) <= 0 {
			record.SetError("empty array", order)
			list = append(list, record)
			return list
		}
		for _, val := range r.([]interface{}) {
			list = append(list, addColumnResponseToRecord(record.Copy(), "", order, val, colsResp)...)
		}
		return list
	case map[string]interface{}:
		if len(colsResp) <= 0 {
			for k := range r.(map[string]interface{}) {
				colsResp = append(colsResp, k)
			}
		}
		for _, v := range colsResp {
			key := fmt.Sprintf("%d_%s", order, v)
			record.Headers = append(record.Headers, key)
			column := wpatterns.FindResponseV3(v, r.(map[string]interface{}))
			record.Data[key] = fmt.Sprintf("%v", column)
		}
		list = append(list, record)
	case error:
		record.SetError(r.(error).Error(), order)
		list = append(list, record)
	default:
		record.SetError(fmt.Sprintf("script_api: %T not implemented", r), order)
		return []records.Record{record}
	}
	return list
}

func (s *scriptAPI) Validations() error {
	if s.Name == "" {
		return errors.New("name is required")
	}
	if s.Order == 0 {
		return errors.New("order is required")
	}
	if s.Url == "" {
		return errors.New("url is required")
	}
	if s.Method == "" {
		return errors.New("method is required")
	}
	if len(s.Columns) <= 0 {
		return errors.New("columns is required")
	}
	methodsAvailable := map[string]struct{}{"GET": {}, "POST": {}, "PUT": {}, "DELETE": {}, "PATCH": {}}
	if _, ok := methodsAvailable[s.Method]; !ok {
		return fmt.Errorf("'%s' is not available method", s.Method)
	}
	return nil
}
