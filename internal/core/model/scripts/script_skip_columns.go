package scripts

import (
	"context"
	"encoding/json"
	"errors"
	"gitlab.com/golang_david/csvprocess/internal/core/model/filter"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
	"gitlab.com/golang_david/csvprocess/pkg/wpatterns"
	"gitlab.com/golang_david/csvprocess/pkg/wurub"
	"log"
)

func init() {
	Register("skip_columns", newScriptSkipColumns)
}

type scriptSkipColumns struct {
	Name    string          `json:"name"`
	Order   int             `json:"order"`
	Filters []filter.Filter `json:"filters"`
	Columns []string        `json:"columns"`
	Workers int             `json:"workers"`
}

func newScriptSkipColumns(data []byte) (Script, error) {
	script := scriptSkipColumns{}
	err := json.Unmarshal(data, &script)
	if err != nil {
		return nil, err
	}
	if script.Workers == 0 {
		script.Workers = defaultWorkers
	}
	err = script.validations()
	if err != nil {
		return nil, err
	}
	return &script, nil
}

func (s *scriptSkipColumns) Execute(_ context.Context, inputs <-chan records.Record) <-chan records.Record {
	if inputs == nil {
		panic("scriptSkipColumns -> input is nil")
	}
	listOutputs := make([]<-chan records.Record, 0)
	for i := 0; i < s.Workers; i++ {
		output := make(chan records.Record)
		listOutputs = append(listOutputs, output)
		go func(output chan records.Record) {
			defer wurub.RecoveryFunc()
			defer close(output)
			for input := range inputs {
				s.processColumnsToSkip(input, output)
			}
		}(output)
	}
	return fanIn(listOutputs...)
}

func (s *scriptSkipColumns) processColumnsToSkip(r records.Record, output chan records.Record) {
	for i := range s.Filters {
		header, err := wpatterns.ReplacePatternInString(s.Filters[i].Header, r.Data)
		if err != nil {
			msg := "script_api: cannot buildFilters Header | " + err.Error()
			log.Println(msg)
			r.SetError(msg, s.Order)
			output <- r
			return
		}
		val, err := wpatterns.ReplacePatternInString(s.Filters[i].Value, r.Data)
		if err != nil {
			msg := "script_api: cannot buildFilters Value | " + err.Error()
			log.Println(msg)
			r.SetError(msg, s.Order)
			output <- r
			return
		}
		if filter.CheckData(r.Data, header, s.Filters[i].Operation, val) {
			r.MarkFiltered(s.Order)
			output <- r
			return
		}
	}

	list := make([]string, 0, len(r.Headers))
	for _, header := range r.Headers {
		skip := contained(header, s.Columns)
		if skip {
			delete(r.Data, header)
		} else {
			list = append(list, header)
		}
	}
	r.Headers = list
	r.LastScript = s.Order
	output <- r
}

func (s *scriptSkipColumns) validations() error {
	if s.Name == "" {
		return errors.New("name is required")
	}
	if s.Order == 0 {
		return errors.New("order is required")
	}
	if len(s.Columns) <= 0 {
		return errors.New("columns is required")
	}
	return nil
}
