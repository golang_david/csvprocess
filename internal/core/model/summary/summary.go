package summary

import (
	"fmt"
	"time"
)

var summary summaryProcess

const format = "2006-01-02 15:04:05"

type summaryProcess struct {
	startTime          time.Time
	endTime            time.Time
	configSelected     string
	inputSelected      []string
	outputDirectory    string
	filenameSuccessful *string
	filenameFailed     *string
	filenameFiltered   *string
	filenameLog        *string
	rowsProcess        int
	rowSuccess         int
	rowErrors          int
	rowFiltered        int
}

func SetStartTime(date time.Time) {
	summary.startTime = date
}

func SetEndTime(date time.Time) {
	summary.endTime = date
}

func SetConfigSelected(config string) {
	summary.configSelected = config
}

func SetInputSelected(input string) {
	summary.inputSelected = append(summary.inputSelected, input)
}

func SetOutputDirectory(dir string) {
	summary.outputDirectory = dir
}

func SetFilenameSuccessful(filename string) {
	summary.filenameSuccessful = &filename
}

func GetFilenameSuccessful() string {
	if summary.filenameSuccessful == nil || *summary.filenameSuccessful == "" {
		panic("filenameSuccessful is empty")
	}
	return *summary.filenameSuccessful
}

func SetFilenameFailed(filename string) {
	summary.filenameFailed = &filename
}

func GetFilenameFailed() string {
	if summary.filenameFailed == nil || *summary.filenameFailed == "" {
		panic("filenameFailed is empty")
	}
	return *summary.filenameFailed
}

func SetFilenameFiltered(filename string) {
	summary.filenameFiltered = &filename
}

func GetFilenameFiltered() string {
	if summary.filenameFiltered == nil || *summary.filenameFiltered == "" {
		panic("filenameFiltered is empty")
	}
	return *summary.filenameFiltered
}

func SetFilenameLog(filename string) {
	summary.filenameLog = &filename
}

func GetFilenameLog() string {
	if summary.filenameLog == nil || *summary.filenameLog == "" {
		panic("filenameLog is empty")
	}
	return *summary.filenameLog
}

func SetRowsProcess(rows int) {
	summary.rowsProcess = rows
}

func SetRowsSuccess(rows int) {
	summary.rowSuccess = rows
}

func SetRowsErrors(rows int) {
	summary.rowErrors = rows
}

func SetRowsFiltered(rows int) {
	summary.rowFiltered = rows
}

func GetSummary() string {
	emptyTime := time.Time{}
	if summary.endTime == emptyTime {
		summary.endTime = time.Now()
	}
	elapseTime := summary.endTime.Sub(summary.startTime)
	str := fmt.Sprintf("Process Summary:\n")
	str = str + fmt.Sprintf("\tStart Time: %v\n", summary.startTime.Format(format))
	str = str + fmt.Sprintf("\tFinish Time: %v\n", summary.endTime.Format(format))
	str = str + fmt.Sprintf("\tElapse Time: %v\n", elapseTime)
	str = str + fmt.Sprintf("\tConfig Selected: %s\n", summary.configSelected)
	str = str + fmt.Sprintf("\tInput Selected: %s\n", summary.inputSelected)
	str = str + fmt.Sprintf("\tOutput Directory: %s\n", summary.outputDirectory)
	str = str + fmt.Sprintf("\tTotal Rows Process: %d\n", summary.rowsProcess)
	str = str + fmt.Sprintf("\tRows Success: %d\n", summary.rowSuccess)
	str = str + fmt.Sprintf("\tRows Errors: %d\n", summary.rowErrors)
	str = str + fmt.Sprintf("\tRows Filtered: %d\n", summary.rowFiltered)
	return str
}
