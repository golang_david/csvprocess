package filter

import (
	"errors"
	"fmt"
	"gitlab.com/golang_david/csvprocess/internal/core/model/records"
	"strings"
)

type Filter struct {
	Header    string `yaml:"header"`
	Operation string `yaml:"operation"`
	Value     string `yaml:"value"`
}

func (f *Filter) Validate() error {
	var errs []string
	operationsAvailable := map[string]bool{"==": true, "!=": true, "<>": true, ">": true, ">=": true, "<": true, "<=": true, "in": true}
	if _, ok := operationsAvailable[f.Operation]; !ok {
		errs = append(errs, fmt.Sprintf("'%s' is not available filter.operation", f.Operation))
	}
	if len(errs) > 0 {
		return errors.New(strings.Join(errs, "; "))
	}
	return nil
}

func (f *Filter) ToString() string {
	str := fmt.Sprintf("Filter\n")
	str = str + fmt.Sprintf("\tfilter.header: %s\n", f.Header)
	str = str + fmt.Sprintf("\tfilter.operation: %s\n", f.Operation)
	str = str + fmt.Sprintf("\tfilter.value: %s\n", f.Value)
	return str
}

func (f *Filter) SetDefault() {}

func CheckData(data records.Data, header, operator, value string) bool {
	return !compare(data[header], operator, value)
}

func compare(a, operation, b string) bool {
	switch operation {
	case "==":
		c := strings.Compare(a, b)
		return c == 0
	case "<>", "!=":
		c := strings.Compare(a, b)
		return c != 0
	case ">":
		c := strings.Compare(a, b)
		return c == -1
	case ">=":
		c := strings.Compare(a, b)
		return c <= 0
	case "<":
		c := strings.Compare(a, b)
		return c == 1
	case "<=":
		c := strings.Compare(a, b)
		return c >= 0
	default:
		return false
	}
}
