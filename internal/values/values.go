package values

const (
	Version = "2.1.2"

	// Directories and filenames
	ConfigDirectory       = "./conf"
	EnvironmentDirectory  = "./environments"
	OutputDirectory       = "./output"
	ConfigFilename        = "config.json"
	LogFilename           = "log.log"
	CopyInputFilename     = "original.csv"
	SuccessResultFilename = "%d_success.csv"
	ErrorResultFilename   = "%d_error.csv"
	FilterResultFilename  = "%d_filter.csv"

	// Key names
	KeyIsSimulation = "isSimulation"
	KeyWithSleep    = "withSleep"
	KeyStatus       = "status"
	KeyErrors       = "errors"
)
