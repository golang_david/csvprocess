#!/bin/bash

#Win - 386
#env GOOS=windows GOARCH=386 go build -o ./build/csvprocess_win_386.exe main.go

#Win - AMD64
#env GOOS=windows GOARCH=amd64 go build -o ./build/csvprocess_win_amd64.exe main.go

#Raspberry
#env GOOS=linux GOARCH=arm go build -o ./build/csvprocess_linux_arm main.go

#Linux - ARM64
#env GOOS=linux GOARCH=arm64 go build -o ./build/csvprocess_linux_arm64 main.go

#Linux - AMD64
#env GOOS=linux GOARCH=amd64 go build -o ./build/csvprocess_linux_amd64 main.go

#Linux - 386
#env GOOS=linux GOARCH=386 go build -o ./build/csvprocess_linux_386 main.go

#MAC - amd64 - 64 bits
env GOOS=darwin GOARCH=amd64 go build -o ./build/csvprocess_mac_amd64 main.go

#MAC - 386 - 32 bits
#GOOS=darwin GOARCH=386 go build -o ./build/csvprocess_mac_386 main.go