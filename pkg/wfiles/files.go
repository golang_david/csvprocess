package wfiles

import (
	"io"
	"io/ioutil"
	"log"
	"os"
)

func CloseResource(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.Println("Close Resource ", err)
	}
}

func OpenFile(filename string) (*os.File, error) {
	return os.Open(filename)
}

func ReadAll(filename string) ([]byte, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer CloseResource(file)
	content, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	return content, err
}

func CreateOrOpenFile(filename string) (*os.File, error) {
	return os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
}
