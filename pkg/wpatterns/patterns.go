package wpatterns

import (
	"errors"
	"fmt"
	"log"
	"regexp"
	"strings"
)

const (
	pattern = "{[a-zA-Z0-9. _-]+}"
)

func ReplacePatternInString(text string, values map[string]string) (string, error) {
	if values == nil {
		return text, nil
	}
	newText := text
	reg, err := regexp.Compile(pattern)
	if err != nil {
		log.Printf("Error to replacePattern: %s\n", err.Error())
		return "", err
	}
	match := reg.FindAllString(text, -1)
	if len(match) > 0 {
		for _, val := range match {
			key := strings.ReplaceAll(val, "{", "")
			key = strings.ReplaceAll(key, "}", "")
			key = strings.Trim(key, " ")
			v := FindColumn(key, values)
			if v != "NAN" {
				newText = strings.ReplaceAll(newText, val, fmt.Sprintf("%s", v))
			} else {
				return "", errors.New(fmt.Sprintf("Pattern '%s' not found", val))
			}
		}
	}
	return newText, nil
}

func ReplacePatternInMap(mapRaw map[string]string, values map[string]string) (map[string]string, error) {
	newMap := make(map[string]string, len(mapRaw))
	for key, value := range mapRaw {
		k, err := ReplacePatternInString(key, values)
		if err != nil {
			return nil, err
		}
		v, err := ReplacePatternInString(value, values)
		if err != nil {
			return nil, err
		}
		newMap[k] = v
	}
	return newMap, nil
}

func ReplacePatternInBytes(bodyRaw []byte, values map[string]string) ([]byte, error) {
	var body []byte
	if bodyRaw != nil {
		strBody, err := ReplacePatternInString(string(bodyRaw), values)
		if err != nil {
			return nil, err
		}
		body = []byte(strBody)
	}
	return body, nil
}

func FindColumn(field string, collection map[string]string) string {
	if val, ok := collection[field]; ok {
		return val
	} else {
		return "NAN"
	}
}

func FindResponseV3(field string, object interface{}) interface{} {
	if field == "" {
		return object
	}
	key := field
	index := strings.Index(field, ".")
	if index != -1 {
		key = field[:index]
	}
	switch object.(type) {
	case map[string]interface{}:
		collection := object.(map[string]interface{})
		if val, ok := collection[key]; ok {
			if index != -1 {
				return FindResponseV3(field[index+1:], val)
			} else {
				return val
			}
		} else {
			return fmt.Errorf("%s not found", field)
		}
	case []interface{}:
		list := make([]string, 0)
		for _, val := range object.([]interface{}) {
			list = append(list, fmt.Sprintf("%v", FindResponseV3(field[index+1:], val)))
		}
		return fmt.Sprintf("%s", strings.Join(list, "|"))
	default:
		return fmt.Sprintf("type %T not implemented", object)
	}
}

func MergeMaps(a map[string]string, b map[string]string) map[string]string {
	c := make(map[string]string, len(a)+len(b))
	for k, v := range a {
		c[k] = v
	}
	for k, v := range b {
		c[k] = v
	}
	return c
}
