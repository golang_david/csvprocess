package wurub

import (
	"log"
	"runtime/debug"
)

var RecoveryFunc = func() {
	if r := recover(); r != nil {
		log.Println("Recovered from panic")
		log.Println(r.(error).Error())
		log.Println(string(debug.Stack()))
	}
}
