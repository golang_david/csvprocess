package wurub

import (
	"math"
	"sync"
	"time"
)

const (
	defaultRate      = "rpm"
	defaultInit      = 100
	defaultLimit     = 1000
	defaultIncrement = 25
	defaultSeconds   = 30
)

type RateLimiterConfig struct {
	Rate                string `json:"rate"`
	Init                int    `json:"init"`
	Limit               int    `json:"limit"`
	IncrementPercentage int    `json:"increment_percentage"`
	IncrementSeconds    int    `json:"increment_seconds"`
}

type RateLimiter interface {
	Wait() bool
}

type rateLimiter struct {
	rate             time.Duration
	min              float64
	max              float64
	limit            float64
	tokens           float64
	percentIncrement int
	secondsIncrement int
	mu               *sync.Mutex
}

func NewRateLimiter(config RateLimiterConfig) RateLimiter {
	if config.Rate == "" {
		config.Rate = defaultRate
	}
	if config.Init == 0 {
		config.Init = defaultInit
	}
	if config.Limit == 0 {
		config.Limit = defaultLimit
	}
	if config.IncrementPercentage == 0 {
		config.IncrementPercentage = defaultIncrement
	}
	if config.IncrementSeconds == 0 {
		config.IncrementSeconds = defaultSeconds
	}
	if config.Init > config.Limit {
		config.Init = config.Limit
	}
	r := &rateLimiter{
		rate:             time.Second,
		min:              getRPS(config.Rate, config.Init),
		max:              getRPS(config.Rate, config.Limit),
		limit:            getRPS(config.Rate, config.Init),
		tokens:           0,
		percentIncrement: config.IncrementPercentage,
		secondsIncrement: config.IncrementSeconds,
		mu:               &sync.Mutex{},
	}
	go r.incrementLimiter()
	go r.resetTokens()
	return r
}

func (r *rateLimiter) Wait() bool {

	for {
		r.mu.Lock()
		if r.tokens < r.limit {
			r.tokens++
			r.mu.Unlock()
			return true
		}
		r.mu.Unlock()
	}
}

func (r *rateLimiter) incrementLimiter() {
	for {
		time.Sleep(time.Duration(r.secondsIncrement) * time.Second)
		r.mu.Lock()
		//fmt.Printf("\ncheck increment limit %f max %f\n", r.limit, r.max)
		if r.limit == r.max {
			r.mu.Unlock()
			//fmt.Printf("\nfinish increment\n")
			return
		}
		inc := 1.0 + (float64(r.percentIncrement) / 100)
		newBurst := math.Ceil(r.limit * inc)
		if newBurst > r.max {
			newBurst = r.max
		}
		r.limit = newBurst
		r.mu.Unlock()
		//fmt.Printf("\nNew Burst %f \n", newBurst)
	}
}

func (r *rateLimiter) resetTokens() {
	for {
		time.Sleep(r.rate)
		r.mu.Lock()
		r.tokens = 0
		r.mu.Unlock()
	}
}

func getRPS(rate string, val int) float64 {
	switch rate {
	case "rps":
		return float64(val)
	case "rpm":
		return float64(val) / 60
	default:
		return float64(val) / 60
	}
}
