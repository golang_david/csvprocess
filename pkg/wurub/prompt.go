package wurub

import (
	"errors"
	"github.com/manifoldco/promptui"
	"io/ioutil"
)

func SelectDirectory(path string, msg string) (string, error) {
	return selectGeneric(path, msg, true)
}

func SelectFile(path string, msg string) (string, error) {
	return selectGeneric(path, msg, false)
}

func SelectOptions(opts []string, msg string) (string, error) {
	return selectOption(opts, msg)
}

func selectGeneric(path string, message string, isDir bool) (string, error) {
	var options []string
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return "", err
	}
	for _, file := range files {
		if file.IsDir() == isDir {
			options = append(options, file.Name())
		}
	}
	if len(options) == 0 {
		if isDir {
			return "", errors.New("no directories found")
		}
		return "", errors.New("no files found")
	}
	return selectOption(options, message)
}

func selectOption(files []string, message string) (string, error) {
	prompt := promptui.Select{
		Label: message,
		Items: files,
	}
	_, result, err := prompt.Run()
	if err != nil {
		return "", err
	}
	return result, nil
}
