package wurub

import (
	"bytes"
	"context"
	"github.com/go-resty/resty/v2"
	"github.com/jarcoal/httpmock"
	"gitlab.com/golang_david/csvprocess/internal/values"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

const (
	defaultRetries = 1
)

type RestClientConfig struct {
	Retries int `json:"retries"`
}

type RestClient interface {
	Do(ctx context.Context, method string, url string, params map[string]string,
		headers map[string]string, body []byte, mockResponse []byte) (*http.Response, error)
}

type RestClientOptions func(c *restClient)
type restClient struct {
	client      *resty.Client
	rateLimiter RateLimiter
	mu          sync.Mutex
}

func NewRestClient(config RestClientConfig, opts ...RestClientOptions) RestClient {
	if config.Retries == 0 {
		config.Retries = defaultRetries
	}
	client := &restClient{
		client: resty.New().SetRetryCount(config.Retries),
	}
	for _, opt := range opts {
		opt(client)
	}
	return client
}

func (r *restClient) Do(ctx context.Context, method string, url string, params map[string]string,
	headers map[string]string, body []byte, mockResponse []byte) (*http.Response, error) {
	if r.rateLimiter != nil {
		r.rateLimiter.Wait()
	}
	r.checkSimulation(ctx, method, url, mockResponse)
	request := r.client.R().
		SetQueryParams(params).
		SetHeaders(headers)
	if method == "POST" || method == "PUT" || method == "PATCH" {
		request.SetBody(body)
	}
	resp, err := request.Execute(method, url)
	if err != nil {
		return nil, err
	}
	printResponse(resp)
	return resp.RawResponse, nil
}

func (r *restClient) checkSimulation(ctx context.Context, method string, url string, mockResponse []byte) {
	r.mu.Lock()
	defer r.mu.Unlock()
	isSimulation := ctx.Value(values.KeyIsSimulation)
	if isSimulation.(bool) {
		httpmock.ActivateNonDefault(r.client.GetClient())
		httpmock.RegisterResponder(method, url, func(request *http.Request) (*http.Response, error) {
			b := httpmock.NewBytesResponse(200, mockResponse)
			withSleep := ctx.Value(values.KeyWithSleep)
			if val, ok := withSleep.(bool); ok && val {
				min := 10
				max := 150
				s1 := rand.NewSource(time.Now().UnixNano())
				r1 := rand.New(s1)
				d := time.Duration(r1.Intn(max-min) + min)
				time.Sleep(d * time.Millisecond)
			}
			return b, nil
		})
	}
}

func printResponse(resp *resty.Response) {
	if resp == nil {
		return
	}
	status := 0
	var body []byte
	if resp.RawResponse != nil {
		status = resp.RawResponse.StatusCode
		body = resp.Body()
		resp.RawResponse.Body = ioutil.NopCloser(bytes.NewReader(body))
	}
	log.Printf("process new row\nMethod: %s\nUrl: %s\nHeaders: %s\nBody: %s\nStatus: %d\nTime:%v\nResponse: %s\n\n",
		resp.Request.Method,
		resp.Request.URL,
		resp.Request.Header,
		resp.Request.Body,
		status,
		resp.Time(),
		body,
	)
}

func WithRateLimiter(rateLimiter RateLimiter) RestClientOptions {
	return func(c *restClient) {
		c.rateLimiter = rateLimiter
	}
}
