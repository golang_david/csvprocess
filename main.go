package main

import (
	"gitlab.com/golang_david/csvprocess/cmd"
	"gitlab.com/golang_david/csvprocess/dependency"
)

func main() {
	dependency.Init()
	cmd.Init()
}
