# CSV Process
this is a cmd-script to process files csv and make api-call in a pipeline

## Build Script
build bin with `go build` or use `sh scriptBuild.sh`

## Execute Script
- For execute `./csvprocess run`
- For simulate a execution without time response `./csvprocess run -simulation`
- For simulate a execution with a random time response `./csvprocess run -simulation -withTime`

## Structure Folders
This script read a folder `config` for search configurations to execute and folder `enviroments` to load environment vars
```
my_folder:
├───config
│   ├───my_script
│   │   └───config.jspn
│   └───other_script
│       └───config.jspn
├───enviroments
│       │───env_production.jspn
│       └───env_testing.jspn
├───csvprocess_bin
```
## Example config.json

> Some fields support **patterns** to be replaced with values received from the above scripts by example pattern **{1_id}**
> and there are some default patterns to use like that ```{__date__}``` to print date now on format "2022-05-15" and ```{__datetime__}``` to print datetime on format "2022-05-15T19:20:05Z"
### Script CSV
Script for read data from CSV file
```
[
  {
    "name": "load data from csv",
    "enbale": true,
    "order": 1,
    "max_row_process": 0,
    "type": "csv",
    "filters": [
        {
            "header": "some_header",
            "operation": "==",
            "value": "some_value"
        }
    ],
    "columns": ["some_header", "other_header"],
    "input_directory": "./data",
    "has_headers": true,
    "delimiter": ","
  }
]
```
- **name**: name of script, only visual, **required**
- **enable**: enable script, **optional**, default true
- **order**: order execution script, **required**
- **max_row_process**: rows to read from csv o another script, **optional**, default 0
- **type**: identify is a csv, **required**
- **filters**: filters for exclude rows, the condition must be true to pass, **optional**, **admits pattern**, operations available ["==", "!=", "<>", ">", ">=", "<", "<=", "in"]
- **columns**: list of header for export from csv, **optional**, default export all headers
- **input_directory**: path directory where search csv to process, **optional** if exist input_file
- **input_file**: path file to process, **optional** if exist input_directory
- **has_headers**: the csv have header or not, **optional**, default false
- **delimiter**: delimiter of csv, **required**
- **workers**: workers for execute concurrency, **optional**, default NroCPU * 4

### Script Api
Script for read data from response api-call
```
[
  {
    "name": "load data from api-call",
    "enable": true,
    "order": 1,
    "max_row_process": 0,
    "type": "api",
    "filters": [
        {
            "header": "some_header",
            "operation": "==",
            "value": "some_value"
        }
    ],
    "columns": ["id", "address.street"],
    "url": "https://some_url/persons",
    "method": "POST",
    "body_template": "./conf/body.json",
    "mock_response": "./conf/mock.json",
    "headers": {
      "content-type": "application/json"
    },
    "params": {
      "size": 10,
      "page": 1
    },
    "rest_client_config": {
      "retries": 1
    },
    "rate_limiter_config": {
      "rate": "rpm",
      "init": 100,
      "limit": 1000,
      "increment_percentage": 25,
      "increment_seconds": 10,
    }
  }
]
```
- **name**: name of script, only visual, **required**
- **enable**: enable script, **optional**, default true
- **order**: order execution script, **required**
- **max_row_process**: rows to read from response o another script, **optional**, default 0
- **type**: identify is an api-call, **required**
- **filters**: filters for exclude rows, the condition must be true to pass, **optional**, **admits pattern**, operations available ["==", "!=", "<>", ">", ">=", "<", "<=", "in"]
- **columns**: list of fields for export from response, **required**, support **concatenation** of fields by example person.address.country.id
- **url**: url from service, **required**, **admits pattern**
- **method**: verb rest, **required**
- **body_directory**: path directory where search body to process, **optional** if exist body_template
- **body_template**: path from body, **optional** if exist body_directory, **admits pattern inside json**
- **mock_response**: path from mock response, **required only if simulation**, **admits pattern inside json**
- **headers**: list of headers, **optional**, **admits pattern**
- **params**: list of params, **optional**, **admits pattern**
- **root_list**: field used as root, **optional**
- **rest_client_config**: configurations rest client, **optional**
  - **retries**: number of retries if failed request, **optional**, default 1
- **rate_limiter_config**: configurations rate limiter, **optional**
  - **rate**: rate request, **optional**, default rpm, available rps and rpm
  - **init**: number of requests you start with, **optional**, default 500
  - **limit**: number of requests you finish with, **optional**, default 1000
  - **increment_percentage**: percentage increment number request, **optional**, default 10%
  - **increment_seconds**: seconds between increments, **optional**, default 60 seg
- **workers**: workers for execute concurrency, **optional**, default NroCPU * 4

### Script JSON
Script for read data from JSON file
```
[
  {
    "name": "load data from json",
    "enable": true,
    "order": 1,
    "max_row_process": 0,
    "type": "json",
    "filters": [
        {
            "header": "some_header",
            "operation": "==",
            "value": "some_value"
        }
    ],
    "columns": ["some_att", "other_att"],
    "input_directory": "./data"
  }
]
```
- **name**: name of script, only visual, **required**
- **enable**: enable script, **optional**, default true
- **order**: order execution script, **required**
- **max_row_process**: rows to read from json o another script, **optional**, default 0
- **type**: identify is a json, **required**
- **filters**: filters for exclude rows, the condition must be true to pass, **optional**, **admits pattern**, operations available ["==", "!=", "<>", ">", ">=", "<", "<=", "in"]
- **columns**: list of attributes for export from json, **optional**, default export all attributes
- **input_directory**: path directory where search json to process, **optional** if exist input_file
- **input_file**: path file to process, **optional** if exist input_directory
- **workers**: workers for execute concurrency, **optional**, default NroCPU * 4

### Script Skip_Columns
Script for remove columns to export in csv final
```
[
  {
    "name": "skip columns to export",
    "enable": true,
    "order": 1,
    "max_row_process": 0,
    "type": "skip_columns",
    "filters": [
        {
            "header": "some_header",
            "operation": "==",
            "value": "some_value"
        }
    ],
    "columns": ["some_col", "other_col"]
  }
]
```
- **name**: name of script, only visual, **required**
- **enable**: enable script, **optional**, default true
- **order**: order execution script, **required**
- **type**: identify is a skip_columns, **required**
- **filters**: filters for exclude rows, the condition must be true to pass, **optional**, **admits pattern**, operations available ["==", "!=", "<>", ">", ">=", "<", "<=", "in"]
- **columns**: list of fields to remove, **required**
- **workers**: workers for execute concurrency, **optional**, default NroCPU * 4

###Example Execution Scripts
```
my_folder:
├───config
│   └───example_script
│       └───config.json
├───enviroments
│   ├───staging.json
│   └───production.json
├───data
│   └───data.csv
├───output
│   ├───2_filter.csv
│   ├───3_error.csv
│   ├───3_success.csv
│   └───log.log
├───csvprocess_bin
```
staging.json
```
{
  "service-one": "https://some_url_to_staging.com",
  "service-two": "https://other_url_to_staging.com"
}
```
production.json
```
{
  "service-one": "https://some_url_to_production.com",
  "service-two": "https://other_url_to_production.com"
}
```
data.csv
```
name,dni
dave,123
john,456
mary,789
```
config.json:
```
[
  {
    "name": "load csv with dni people",
    "order": 1,
    "type": "csv",
    "input_directory": "./data",
    "has_headers": true,
    "delimiter": ","
  },
  {
    "name": "search people by dni",
    "order": 2,
    "type": "api",
    "columns": ["id"],
    "filters": [
        {
            "header": "1_dni",
            "operation": "!=",
            "value": "123"
        }
    ],
    "url": "{service-one}/person",
    "method": "GET",
    "params": {
      "dni": "{1_dni}"
    }
  },
  {
    "name": "search country of people",
    "order": 3,
    "type": "api",
    "columns": ["name"],
    "url": "{service-two}/person/{2_id}/country",
    "method": "GET"
  }
]
```
####result:
- 2_filter.csv
```
1_name,1_dni
dave,123
```
- 3_success.csv
```
1_name,1_dni,2_status,2_id,3_status,3_name
mary,789,200,3,200,ARGENTINA
```
- 3_error.csv
```
1_name,1_dni,2_status,2_id,3_status,3_name
john,456,200,2,500,some_error_from_body_response
```
