package dependency

import (
	"gitlab.com/golang_david/csvprocess/internal/core/services"
	"gitlab.com/golang_david/csvprocess/internal/ports"
)

var (
	Service ports.Service
)

func Init() {
	Service = services.NewService()
}
