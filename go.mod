module gitlab.com/golang_david/csvprocess

go 1.15

require (
	github.com/go-resty/resty/v2 v2.6.0
	github.com/jarcoal/httpmock v1.0.8
	github.com/manifoldco/promptui v0.8.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
)
