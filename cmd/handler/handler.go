package handler

import (
	"gitlab.com/golang_david/csvprocess/internal/ports"
	"log"
)

type Handler struct {
	service ports.Service
}

func NewHandler(srv ports.Service) Handler {
	return Handler{service: srv}
}

func (h Handler) Run(isSimulation bool, withTime bool) {
	err := h.service.Run(isSimulation, withTime)
	if err != nil {
		log.Println(err)
	}
	return
}
