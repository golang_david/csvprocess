package cmd

import (
	"flag"
	"fmt"
	"gitlab.com/golang_david/csvprocess/cmd/handler"
	"gitlab.com/golang_david/csvprocess/dependency"
	"gitlab.com/golang_david/csvprocess/internal/values"
	"os"
)

func Init() {
	runCommand := flag.NewFlagSet("run", flag.ContinueOnError)
	simulationPtr := runCommand.Bool("simulation", false, "simulated execution")
	withTimePtr := runCommand.Bool("withTime", false, "simulated with time delay execution")

	if len(os.Args) <= 1 {
		fmt.Println("command not found")
		help()
		return
	}
	switch os.Args[1] {
	case "run":
		if err := runCommand.Parse(os.Args[2:]); err == nil {
			h := handler.NewHandler(dependency.Service)
			h.Run(*simulationPtr, *withTimePtr)
		}
	case "-version", "--version", "-v":
		version()
	case "-help", "--help", "-h":
		help()
	default:
		fmt.Printf("unknown command '%s'\n", os.Args[1])
		help()
	}
}

func version() {
	fmt.Printf("csvprocess version %s\n", values.Version)
}
func help() {
	version()
	fmt.Printf("usage: csvprocess [-version | --version | -v] [-help | --help | -h] <command> [<flag>]\n")
	fmt.Printf("Comands available:\n")
	fmt.Printf("\trun: execute processing\n")
	fmt.Printf("\trun -simulation: simulate a execution without response time\n")
	fmt.Printf("\trun -simulation -withTime: simulate a execution with variable response time\n")
	fmt.Printf("\nFor more info visit https://gitlab.com/golang_david/csvprocess\n")
}
