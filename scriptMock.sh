#!/bin/bash

#go get github.com/golang/mock v1.5.0

#mockgen -source=./internal/ports/interfaces.go -package=mockups -destination=./mocks/mockups/interfaces.go
mockgen -source=./pkg/files/files.go -package=mockups -destination=./mocks/mockups/files.go
mockgen -source=./pkg/prompt/prompt.go -package=mockups -destination=./mocks/mockups/prompt.go
mockgen -source=./pkg/restcli/restcli.go -package=mockups -destination=./mocks/mockups/restcli.go